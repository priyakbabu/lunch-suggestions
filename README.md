# Lunch Suggestion App

Visit [here](https://lunch-suggestion-1a625.web.app/) for the hosted project.

## Tasks completed

1. Simple lunch suggestion form created.
2. Validation implemented for fields.
3. Integrated with email service (mailgun).
4. Snackbar implemented for success and failure scenario.
5. Implemented responsive design

## Assumptions

1. Mailgun is used for email service.
2. Team lunch group email id is pre-configured. In this case, email gets sent to priyakbabu@gmail.com.
3. To replicate error scenario, set the name field to "error".

## Improvements

1. API key for mailgun is stored within the code for testing purpose. In a real world environment, it will be stored on a backend server.
2. Testcases needs to be implemented.
3. To be dockerized.
4. Colour codes and styling can be improved.
