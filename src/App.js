import classes from './App.module.css';
import Banner from './components/Banner/Banner';
import SuggestionForm from './components/SuggestionForm/SuggestionForm';

function App() {
  return (
    <div className={classes.container}>
      <Banner />
      <SuggestionForm />
    </div>
  );
}

export default App;
