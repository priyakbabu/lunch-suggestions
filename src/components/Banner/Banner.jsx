import classes from './Banner.module.css';

const Banner = () => {
  return <div className={classes.banner}>Lunch Suggestion</div>;
};

export default Banner;
