import classes from './SuggestionForm.module.css';
import Input from '../../ui-components/Input/Input';
import TextArea from '../../ui-components/TextArea/TextArea';
import Button from '../../ui-components/Button/Button';
import useInput from '../../hooks/use-input';
import { nameFieldValidator, dateFieldValidator, restaurantFieldValidator } from '../../validators/form-validator';
import { triggerEmail, STATUS_SUCCESS } from '../../support/util';
import Snackbar from '../../ui-components/Snackbar/Snackbar';
import useSnackbar from '../../hooks/use-snackbar';

const SuggestionForm = () => {
  const nameInput = useInput({ validator: nameFieldValidator });
  const dateInput = useInput({ validator: dateFieldValidator });
  const restaurantInput = useInput({ validator: restaurantFieldValidator });
  const commentsInput = useInput(true);
  const snackbar = useSnackbar();

  const isFormInvalid =
    !nameInput.enteredValueIsValid || !dateInput.enteredValueIsValid || !restaurantInput.enteredValueIsValid;

  const resetForm = () => {
    nameInput.reset();
    dateInput.reset();
    restaurantInput.reset();
    commentsInput.reset();
  };

  const formSubmitHandler = (event) => {
    event.preventDefault();

    nameInput.validate();
    dateInput.validate();
    restaurantInput.validate();

    if (isFormInvalid) {
      return;
    }

    triggerEmail({
      date: dateInput.enteredValue,
      restaurant: restaurantInput.enteredValue,
      name: nameInput.enteredValue,
      comments: commentsInput.enteredValue,
    }).then((status) => {
      snackbar.setShowSnackbar(true);
      if (status === STATUS_SUCCESS) {
        snackbar.setType('success');
        snackbar.setMessage('Email submitted successfully.');
      } else {
        snackbar.setType('failure');
        snackbar.setMessage('Email submission failed.');
      }
      setTimeout(() => {
        snackbar.reset();
      }, 2000);
    });

    resetForm();
  };

  return (
    <>
      {snackbar.showSnackbar && <Snackbar type={snackbar.type} message={snackbar.message} />}
      <form onSubmit={formSubmitHandler} className={classes.lunchForm}>
        <Input
          label='Name*'
          inputProps={{
            id: 'name',
            value: nameInput.enteredValue,
            onChange: nameInput.onChangeHandler,
            onBlur: nameInput.onTouchHandler,
          }}
          isInvalid={nameInput.isInputInvalid}
          errorMessage={nameInput.errorMessage}
        />
        <Input
          label='Date*'
          inputProps={{
            id: 'date',
            type: 'date',
            value: dateInput.enteredValue,
            onChange: dateInput.onChangeHandler,
            onBlur: dateInput.onTouchHandler,
          }}
          isInvalid={dateInput.isInputInvalid}
          errorMessage={dateInput.errorMessage}
        />
        <Input
          label='Restaurant*'
          inputProps={{
            id: 'restaurant',
            value: restaurantInput.enteredValue,
            onChange: restaurantInput.onChangeHandler,
            onBlur: restaurantInput.onTouchHandler,
          }}
          isInvalid={restaurantInput.isInputInvalid}
          errorMessage={restaurantInput.errorMessage}
        />
        <TextArea
          label='Comments'
          textareaProps={{
            id: 'comments',
            value: commentsInput.enteredValue,
            onChange: commentsInput.onChangeHandler,
          }}
        />
        <Button name='Submit' buttonProps={{ disabled: isFormInvalid }} />
      </form>
    </>
  );
};

export default SuggestionForm;
