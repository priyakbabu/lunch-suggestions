import { useEffect, useState } from 'react';

const useInput = ({ isOptional = false, validator } = {}) => {
  const [enteredValue, setEnteredValue] = useState('');
  const [enteredValueIsValid, setEnteredValueIsValid] = useState(false);
  const [enteredValueTouched, setEnteredValueTouched] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [isInputInvalid, setIsInputInvalid] = useState(true);

  useEffect(() => {
    setIsInputInvalid(!isOptional && !enteredValueIsValid && enteredValueTouched);
  }, [isOptional, enteredValueIsValid, enteredValueTouched]);

  const onChangeHandler = (event) => {
    const value = event.target.value;
    setEnteredValueIsValid(true);
    if (value && value.trim().length > 1) {
      validate(value);
    }
    setEnteredValue(event.target.value);
  };

  const onTouchHandler = () => {
    validate();
    setEnteredValueTouched(true);
  };

  const validate = (value = enteredValue) => {
    if (!isOptional && validator) {
      const result = validator(value);
      setEnteredValueIsValid(result.isValid);
      setIsInputInvalid(!result.isValid);
      setErrorMessage(result.errorMessage);
      return !result.isValid;
    }
  };

  const reset = () => {
    setEnteredValue('');
    setEnteredValueIsValid(false);
    setEnteredValueTouched(false);
  };

  return {
    enteredValue,
    setEnteredValue,
    isInputInvalid,
    errorMessage,
    enteredValueIsValid,
    setEnteredValueIsValid,
    setEnteredValueTouched,
    onTouchHandler,
    onChangeHandler,
    validate,
    reset,
  };
};

export default useInput;
