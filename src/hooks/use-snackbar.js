import { useState } from 'react';

const useSnackbar = () => {
  const [showSnackbar, setShowSnackbar] = useState(false);
  const [type, setType] = useState('');
  const [message, setMessage] = useState('');

  const reset = () => {
    setShowSnackbar(false);
    setType('');
    setMessage('');
  };

  return { showSnackbar, setShowSnackbar, type, setType, message, setMessage, reset };
};

export default useSnackbar;
