import mailgun from 'mailgun-js';

export const STATUS_SUCCESS = 'SUCCESS';
export const STATUS_ERROR = 'ERROR';

export const triggerEmail = async (data) => {
  // Note: This piece of code needs to be deployed in a server to secure API key.

  const api_key = 'key-6c2d67f70e0bb8de3483fae1fd16158f'; // Temporary API key
  const DOMAIN = 'sandboxca46872139a646b8a748f0c0f4232ad5.mailgun.org';
  if (data.name.includes('error')) {
    return STATUS_ERROR; // TESTING - For simulating error scenario
  }

  const mg = mailgun({ apiKey: api_key, domain: DOMAIN });

  const payload = {
    from: 'no-reply@lunchsuggest.com',
    to: 'priyakbabu@gmail.com',
    subject: 'Lunch suggestion',
    html: `<h3>Lunch Suggestion</h3>
    <br>
    <div>Date: ${data.date}</div>
    <div>Restaurant: ${data.restaurant}</div>
    <div>Suggested by: ${data.name}</div>
    <div>Comments: ${data.comments}</div>`,
  };
  const result = await mg.messages().send(payload);
  return result.id ? STATUS_SUCCESS : STATUS_ERROR;
};
