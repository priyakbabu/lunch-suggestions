import classes from './Button.module.css';

const Button = (props) => {
  const { name, buttonProps = {} } = props;
  return (
    <button className={classes.primaryBtn} {...buttonProps}>
      {name}
    </button>
  );
};

export default Button;
