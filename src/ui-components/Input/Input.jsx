import classes from './Input.module.css';

const Input = (props) => {
  const { label = '', isInvalid = true, errorMessage = 'Invalid input', inputProps = {} } = props;

  const inputClass = `${classes.input} ${inputProps.type === 'date' ? classes.input_date : ''} ${
    isInvalid ? classes.input_invalid : ''
  }`;

  return (
    <div className={classes.container}>
      <label htmlFor={inputProps.id} className={classes.label}>
        {label}
      </label>
      <div className={classes.input_container}>
        <input className={inputClass} {...inputProps}></input>
        {isInvalid && (
          <div className={classes.error_container}>
            <div className={classes.invalid}>{errorMessage}</div>
          </div>
        )}
      </div>
    </div>
  );
};

export default Input;
