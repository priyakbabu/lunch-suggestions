import classes from './Snackbar.module.css';

const Snackbar = ({ type, message = '' }) => {
  return (
    <div className={classes.snackbar}>
      <div className={`${type === 'success' ? classes.success : classes.failure}`}>{message}</div>
    </div>
  );
};

export default Snackbar;
