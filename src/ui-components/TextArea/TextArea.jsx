import classes from './TextArea.module.css';

const TextArea = (props) => {
  const { label, textareaProps = {} } = props;

  return (
    <div className={classes.container}>
      <label htmlFor={textareaProps.id} className={classes.label}>
        {' '}
        {label}{' '}
      </label>
      <div className={classes.textarea_container}>
        <textarea className={classes.textarea} {...textareaProps} />
      </div>
    </div>
  );
};

export default TextArea;
