export const nameFieldValidator = (value) => {
  let message = '';
  if (!value || value.trim().length === 0) {
    message = 'Name is mandatory';
  } else if (value.trim().length < 2) {
    message = 'Minimum 2 characters';
  }
  const result = {
    isValid: message.length === 0,
    errorMessage: message,
  };
  return result;
};

export const dateFieldValidator = (value) => {
  let message = '';
  if (!value || value.trim().length === 0) {
    message = 'Date is mandatory';
  } else {
    const enteredDate = new Date(new Date(value).setHours(23, 59, 59, 999));
    const now = new Date(new Date().setHours(0, 0, 0, 0));
    const future = new Date(new Date().setHours(23, 59, 59, 999));
    future.setDate(future.getDate() + 60);
    if (enteredDate < now) {
      message = 'Enter a future date';
    } else if (enteredDate > future) {
      message = 'Enter a date less than 60 days';
    }
  }
  const result = {
    isValid: message.length === 0,
    errorMessage: message,
  };
  return result;
};

export const restaurantFieldValidator = (value) => {
  let message = '';
  if (!value || value.trim().length === 0) {
    message = 'Restaurant is mandatory';
  } else if (value.trim().length < 2) {
    message = 'Minimum 2 characters';
  }
  const result = {
    isValid: message.length === 0,
    errorMessage: message,
  };
  return result;
};
